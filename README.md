# S10-Securite

## Download project

Download the project (git clone or zip download) and open a cmd prompt at its base directory (you should see the README.md in this directory)

## Requirements

1. Create a Python 3 virutal environment

```Shell
Windows\S10-securite python -m venv venv
```

2. Activate the venv

```Shell
Windows\S10-securite venv\Scripts\activate.bat
```

3. Install the requirements

```Shell
(venv) Windows\S10-securite pip install -r requirements.txt
```

## Setup & Launch

1. Make migrations
```Shell
(venv) Windows\S10-securite\securite$ python manage.py makemigrations
```

2. Migrate DB
```Shell
(venv) Windows\S10-securite\securite$ python manage.py migrate
```

3. Run server
```Shell
(venv) Windows\S10-securite\securite$ python manage.py runserver
```

## Usage

1. Create a superuser 
```Shell
(venv) Windows\S10-securite\securite$ python manage.py createsuperuser
```

2. Go to http://localhost:8000/admin/ to login as a superuser and create an admin user (is_admin = true)

3. Login at http://localhost:8000/adm/login for admin users

4. Register as a regular user at http://localhost:8000/register/ to go shopping!

5. Login at http://localhost:8000/login for regular users
## Tests
1. Run tests
```Shell
(venv) Windows\S10-securite\securite$ python manage.py test
```

1. Write more tests
```Shell
S10-securite\securite\app\test.py
```

## Structure
Routes
```Shell
TurboStock\securite\app\urls.py
```

Views( controllers )
```Shell
S10-securite\securite\app\views\
```
Models
```Shell
S10-securite\securite\app\models\
```

Templates ( html )
```Shell
S10-securite\securite\app\template\
```

Authentication system
```Shell
S10-securite\securite\app\authentication\
```
