from django.apps import AppConfig


class PaymentBackendConfig(AppConfig):
    name = 'payment_backend'
