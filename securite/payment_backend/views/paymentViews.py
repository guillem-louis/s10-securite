from Crypto.PublicKey import RSA
from cryptography.hazmat.primitives import serialization, hashes
from cryptography.hazmat.primitives.asymmetric.rsa import RSAPublicKey
from cryptography.hazmat.primitives.serialization import load_pem_public_key
from cryptography.hazmat.primitives.serialization import load_der_public_key
from django.http import HttpResponse
from django.views.decorators.csrf import csrf_exempt
from rest_framework import status

from rest_framework.decorators import api_view
from rest_framework.response import Response

from cryptography.hazmat.backends import default_backend
from cryptography.hazmat.primitives.asymmetric import rsa, padding

PRIVATE_KEY = rsa.generate_private_key(
    public_exponent=65537,
    key_size=2048,
    backend=default_backend()
)
PUBLIC_KEY = PRIVATE_KEY.public_key()


@csrf_exempt
def pay(request):

    cryptedFile = request.FILES['file']
    crypted = cryptedFile.read()
    print(crypted)
    card_number = PRIVATE_KEY.decrypt(
        crypted,
        padding.OAEP(
            mgf=padding.MGF1(algorithm=hashes.SHA256()),
            algorithm=hashes.SHA256(),
            label=None
        )
    ).decode()
    print(card_number)
    if(card_number == "666"):
        return HttpResponse("", status=200)
    else:
        return HttpResponse("", status=400)



@api_view(['GET'])
def getPublicKey(request):
    pemp = PUBLIC_KEY.public_bytes(
        encoding=serialization.Encoding.PEM,
        format=serialization.PublicFormat.SubjectPublicKeyInfo
    )
    return Response(pemp)
