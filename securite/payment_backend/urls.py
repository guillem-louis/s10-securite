from django.urls import include, path
from payment_backend.views import *

# Wire up our API using automatic URL routing.
# Additionally, we include login URLs for the browsable API.

urlpatterns = [

    path('payment/pay/', paymentViews.pay),
    path('payment/key/', paymentViews.getPublicKey)
]