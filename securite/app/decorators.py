from django.contrib.auth.decorators import login_required
from django.contrib.auth.models import AnonymousUser
from django.http import HttpResponseRedirect
from django.shortcuts import render


def auth_required(function):
    def wrapper(request, *args, **kw):
        user = request.user
        if not user.is_authenticated:
            request.user = AnonymousUser()
            return render(request, 'user/login.html', status=401)
        else:
            return function(request, *args, **kw)

    return wrapper


def admin_required(function):
    def wrapper(request, *args, **kw):
        user = request.user
        if not user.is_authenticated or not user.is_admin:
            request.user = AnonymousUser()
            return render(request, 'admin/adminLogin.html', status=401)
        else:
            return function(request, *args, **kw)

    return wrapper
