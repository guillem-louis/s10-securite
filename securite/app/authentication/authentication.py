from app.models import *

def is_admin(user):
    return user.is_admin
