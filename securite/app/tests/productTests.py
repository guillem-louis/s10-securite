from django.contrib.auth.models import AnonymousUser
from django.test import TestCase, RequestFactory
from app.views import *
from .utils import *
from app.services import *


class ProductTestCase(TestCase):

    def setUp(self):
        self.factory = RequestFactory()
        self.user = User.objects.create(first_name=randomword(8), last_name=randomword(8), email=randomword(8),
                                        password=randomword(8), is_admin=False)
        self.admin = User.objects.create(first_name=randomword(8), last_name=randomword(8), email=randomword(8),
                                         password=randomword(8), is_admin=True)
        self.product = Product.objects.create(name=randomword(8), category=randomword(8), unit_price=str(1000))
        self.product2 = Product.objects.create(name="Cool product", category="Tech", unit_price=30.00)
        self.product3 = Product.objects.create(name="Nice product", category="Cloth", unit_price=100.00)

    # tests product create service

    def test_create_product_service_ok(self):
        product_id = create_product_service(randomword(8), randomword(8), str(10000))
        self.assertNotEqual(product_id, -1)

    def test_create_product_service_empty_field(self):
        with self.assertRaises(ValueError):
            create_product_service("", randomword(8), str(10000))
        with self.assertRaises(ValueError):
            create_product_service(randomword(8), "", str(10000))
        with self.assertRaises(ValueError):
            create_product_service(randomword(8), randomword(8), "")

    def test_create_product_service_invalid_unit_price(self):
        with self.assertRaises(ValueError):
            create_product_service(randomword(8), randomword(8), str(-1))
        with self.assertRaises(ValueError):
            create_product_service(randomword(8), randomword(8), randomword(8))

    # test new product page access

    def test_new_product_view_ok(self):
        request = self.factory.get('/adm/product/new')
        request.user = self.admin
        response = new_product(request)
        self.assertEqual(response.status_code, 200)

    def test_new_product_view_not_authenticated(self):
        request = self.factory.get('/adm/product/new')
        request.user = AnonymousUser()
        response = new_product(request)
        self.assertEqual(response.status_code, 401)

    def test_new_product_view_not_admin(self):
        request = self.factory.get('/adm/product/new')
        request.user = self.user
        response = new_product(request)
        self.assertEqual(response.status_code, 401)

    # test create product views

    def test_create_product_ok(self):
        request = self.factory.post('adm/product/create/',
                                    {"name": randomword(8), "category": randomword(8), "unit_price": "1000"})
        request.user = self.admin
        response = create_product(request)
        self.assertEqual(response.status_code, 200)

    def test_create_product_not_authenticated(self):
        request = self.factory.post('adm/product/create/',
                                    {"name": randomword(8), "category": randomword(8), "unit_price": "1000"})
        request.user = AnonymousUser()
        response = create_product(request)
        self.assertEqual(response.status_code, 401)

    def test_create_product_not_admin(self):
        request = self.factory.post('adm/product/create/',
                                    {"name": randomword(8), "category": randomword(8), "unit_price": "1000"})
        request.user = self.user
        response = create_product(request)
        self.assertEqual(response.status_code, 401)

    def test_create_product_empty_form(self):
        request = self.factory.post('adm/product/create/', {})
        request.user = self.admin
        response = create_product(request)
        self.assertEqual(response.status_code, 400)

    # Test update product service

    def test_update_product_service_ok(self):
        updated = update_product_service(self.product.id, randomword(8), randomword(8), str(10000))
        self.assertTrue(updated)

    def test_update_product_service_empty_field(self):
        with self.assertRaises(ValueError):
            update_product_service(self.product.id, "", randomword(8), str(10000))
        with self.assertRaises(ValueError):
            update_product_service(self.product.id, randomword(8), "", str(10000))
        with self.assertRaises(ValueError):
            update_product_service(self.product.id, randomword(8), randomword(8), "")

    def test_update_product_service_invalid_unit_price(self):
        with self.assertRaises(ValueError):
            update_product_service(self.product.id, randomword(8), randomword(8), str(-1))
        with self.assertRaises(ValueError):
            update_product_service(self.product.id, randomword(8), randomword(8), randomword(8))

    def test_update_product_inexistant(self):
        updated = update_product_service(-12, randomword(8), randomword(8), str(1000))
        self.assertFalse(updated)

    # test product update page access

    def test_new_product_view_ok(self):
        request = self.factory.get('/adm/product/' + str(self.product.id))
        request.user = self.admin
        response = product(request, self.product.id)
        self.assertEqual(response.status_code, 200)

    def test_new_product_view_ok(self):
        request = self.factory.get('/adm/product/999999999')
        request.user = self.admin
        response = product(request, 999999999)
        self.assertEqual(response.status_code, 400)

    def test_new_product_view_not_authenticated(self):
        request = self.factory.get('/adm/product/' + str(self.product.id))
        request.user = AnonymousUser()
        response = product(request, self.product.id)
        self.assertEqual(response.status_code, 401)

    def test_new_product_view_not_admin(self):
        request = self.factory.get('/adm/product/' + str(self.product.id))
        request.user = self.user
        response = product(request, self.product.id)
        self.assertEqual(response.status_code, 401)

    # test update product views

    def test_update_product_ok(self):
        request = self.factory.post('adm/product/' + str(self.product.id) + '/update/',
                                    {"name": randomword(8), "category": randomword(8), "unit_price": "1000"})
        request.user = self.admin
        response = update_product(request, self.product.id)
        self.assertEqual(response.status_code, 200)

    def test_update_product_not_authenticated(self):
        request = self.factory.post('adm/product/' + str(self.product.id) + '/update/',
                                    {"name": randomword(8), "category": randomword(8), "unit_price": "1000"})

        request.user = AnonymousUser()
        response = update_product(request, self.product.id)
        self.assertEqual(response.status_code, 401)

    def test_update_product_not_admin(self):
        request = self.factory.post('adm/product/' + str(self.product.id) + '/update/',
                                    {"name": randomword(8), "category": randomword(8), "unit_price": "1000"})
        request.user = self.user
        response = update_product(request, self.product.id)
        self.assertEqual(response.status_code, 401)

    def test_update_product_empty_form(self):
        request = self.factory.post('adm/product/' + str(self.product.id) + '/update/', {})
        request.user = self.admin
        response = update_product(request, self.product.id)
        self.assertEqual(response.status_code, 400)

    # test search

    def test_search_product_service_ok(self):
        result = search_product_service(name="Cool", price_min ="", price_max="", category="")
        result_name = [product.name for product in result]
        self.assertEqual(len(result_name), 1)
        self.assertEqual(result_name[0], "Cool product")

    def test_search_product_service_boundries_ok(self):
        result = search_product_service(name="", price_min ="20", price_max="100", category="")
        result_name = [product.name for product in result]
        self.assertEqual(len(result_name), 2)
        self.assertEqual(result_name[0], "Cool product")
        self.assertEqual(result_name[1], "Nice product")

    def test_search_product_service_category_ok(self):
        result = search_product_service(name="", price_min="", price_max="", category="Tech")
        result_name = [product.name for product in result]
        self.assertEqual(len(result_name), 1)
        self.assertEqual(result_name[0], "Cool product")
