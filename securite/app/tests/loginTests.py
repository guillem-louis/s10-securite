from django.contrib.auth.models import AnonymousUser
from django.test import TestCase, RequestFactory
from app.views import *
from .utils import *


class AuthenticationTestCase(TestCase):

    def setUp(self):
        self.factory = RequestFactory()
        self.user = User.objects.create(first_name=randomword(8), last_name=randomword(8), email=randomword(8),
                                        password=randomword(8), is_admin=False)
        self.admin = User.objects.create(first_name=randomword(8), last_name=randomword(8), email=randomword(8),
                                         password=randomword(8), is_admin=True)

    def test_authentication_ok(self):
        response = self.client.post('/authenticate/', {"login": self.user.email, "password": self.user.password})
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.context["user"].id, self.user.id)

        response = self.client.post('/authenticate/', {"login": self.admin.email, "password": self.admin.password})
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.context["user"].id, self.admin.id)

    def test_authentication_empty_form(self):
        response = self.client.post('/authenticate/', {})
        self.assertEqual(response.status_code, 400)

    def test_authentication_wrong_email(self):
        response = self.client.post('/authenticate/', {"login": randomword(9), "password": self.user.password})
        self.assertEqual(response.status_code, 401)
        self.assertEqual(response.context["user"], AnonymousUser())

        response = self.client.post('/authenticate/', {"login": randomword(9), "password": self.admin.password})
        self.assertEqual(response.status_code, 401)
        self.assertEqual(response.context["user"], AnonymousUser())

    def test_authentication_wrong_password(self):
        response = self.client.post('/authenticate/', {"login": self.user.email, "password": randomword(9)})
        self.assertEqual(response.status_code, 401)
        self.assertEqual(response.context["user"], AnonymousUser())

        response = self.client.post('/authenticate/', {"login": self.admin.email, "password": randomword(9)})
        self.assertEqual(response.status_code, 401)
        self.assertEqual(response.context["user"], AnonymousUser())

    def test_authenticated_view_authenticated(self):
        request = self.factory.get('/home/')
        request.user = self.user
        response = home(request)
        self.assertEqual(response.status_code, 200)

        request = self.factory.get('/home/')
        request.user = self.admin
        response = home(request)
        self.assertEqual(response.status_code, 200)

    def test_authenticated_view_not_authenticated(self):
        request = self.factory.get('/home/')
        request.user = AnonymousUser()
        response = home(request)
        self.assertEqual(response.status_code, 401)
