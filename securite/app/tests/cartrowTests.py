from django.contrib.auth.models import AnonymousUser
from django.test import TestCase, RequestFactory
from app.views import *
from django.contrib.auth.models import AnonymousUser
from django.test import TestCase, RequestFactory
from app.views import *
from .utils import *
from app.services import *

from .utils import *


class AuthenticationTestCase(TestCase):

    def setUp(self):
        self.factory = RequestFactory()
        self.user = User.objects.create(first_name="test", last_name="test", email="test",
                                        password="test")
        self.cart = Cart.objects.create(user=self.user)
        self.product1 = Product.objects.create(name="product1", category="categoru", unit_price=1)


    def test_addrow_ok(self):
        request = self.factory.post('/addcartrow/', {"product_id": 1})
        request.user = self.user
        response = create_cartrow(request)
        self.assertEqual(response.status_code, 200)

    def test_addrow_productdoesntexist(self):
        request = self.factory.post('/addcartrow/', {"product_id": 3})
        request.user = self.user
        response = create_cartrow(request)
        self.assertEqual(response.status_code, 404)

    def test_addrow_addtwo(self):
        request = self.factory.post('/addcartrow/', {"product_id": 1})
        request2 = self.factory.post('/addcartrow/', {"product_id": 1})
        request.user = self.user
        request2.user = self.user
        response = create_cartrow(request)
        response2 = create_cartrow(request2)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response2.status_code, 200)
        cart = Cart.objects.get(user = self.user)
        cartrow = Cartrow.objects.get(cart=cart,product=self.product1)
        self.assertEqual(cartrow.quantity,2)

    def test_addrow_delete(self):
        request = self.factory.post('/addcartrow/', {"product_id": 1})
        request.user = self.user
        response = create_cartrow(request)
        self.assertEqual(response.status_code, 200)
        cart = Cart.objects.get(user=self.user)
        cartrow = Cartrow.objects.get(cart=cart, product=self.product1)
        request = self.factory.delete('cartrow/' + str(cartrow.id) +'/delete')
        request.user = self.user
        response = delete_cartrow(request, cartrow.id)
        self.assertEqual(response.status_code, 302)
        with self.assertRaises(Cartrow.DoesNotExist):
            Cartrow.objects.get(cart=cart, product=self.product1)


