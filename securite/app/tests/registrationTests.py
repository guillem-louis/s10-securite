import unittest

from django.test import TestCase, RequestFactory
from app.views import *
from .utils import *
from app.services import create_user_service


class RegistrationTest(TestCase):

    def setUp(self):
        self.first_name = randomword(8)
        self.last_name = randomword(8)
        self.email = randomword(8)
        self.password = randomword(8)
        self.is_admin = False

    def test_registration_ok(self):
        response = self.client.post('/user/create', {"email": 'test@test.com',
                                                     "password": "test",
                                                     "first-name": "test",
                                                     "last-name": "test"})
        self.assertEqual(response.status_code, 200)

    def test_registration_ko_empty_form(self):
        response = self.client.post('/user/create', {"email": "",
                                                     "password": "",
                                                     "first-name": "",
                                                     "last-name": ""})
        self.assertEqual(response.status_code, 400)

    def test_registration_service_ok(self):
        response = create_user_service('test', 'test', 'test@test.com', 'test')

        self.assertNotEqual(response, -1)

    def test_registration_service_ko(self):
        with self.assertRaises(ValueError):
            create_user_service('', '', '', '')
