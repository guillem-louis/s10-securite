from django.urls import path

from . import views

urlpatterns = [
    # user URLs
    path('authenticate/', views.auth),
    path('', views.login),
    path('login/', views.login),
    path('logout/', views.logout),
    path('createuser/', views.register),
    path('user/create', views.create),
    path('home/', views.home),
    path('cart/', views.cart, name='cart'),
    path('cartrow/add/', views.home),
    path('addcartrow/', views.create_cartrow),
    path('cartrow/<int:cartrow_id>/update', views.update_cartrow, name='update_cartrow'),
    path('cartrow/<int:cartrow_id>/delete', views.delete_cartrow),
    path('payment/', views.payment),
    path('pay/', views.pay),

    # admin URLs
    path('adm/authenticate/', views.adminAuth),
    path('adm/home/', views.adminHome),
    path('adm/login/', views.adminLogin),
    path('adm/logout/', views.adminLogout),
    path('adm/product/new/', views.new_product),
    path('adm/product/create/', views.create_product),
    path('adm/product/<int:product_id>', views.product),
    path('adm/product/<int:product_id>/update/', views.update_product),

    # product URLs
    path('products', views.list_products),
    path('search/product', views.search_product),
    path('adm/search/product', views.adm_search_product)

]
