from django.db import models


class User(models.Model):
    """
    This class is the user class. It will be used for authentication

    Attributes:
        id (int): The ID of the user that is incremented automatically by Django.
            It is the PK.
        first_name (str): The first name of the user. This field has a maximum length of 50 char.
        last_name (str): The last name of the user. This field has a maximum length of 50 char.
        email (str): The email address of the user. This field has a maximum length of 100 char.
    """
    id = models.AutoField(primary_key=True)
    first_name = models.CharField(max_length=50)
    last_name = models.CharField(max_length=50)
    email = models.CharField(unique=True, max_length=100)
    password = models.CharField(max_length=100)
    last_login = models.CharField(null=True, default=None, max_length=100)
    is_admin = models.BooleanField(default=False)
    objects = models.Manager()

    def is_authenticated(self):
        return True

    def __str__(self):
        return self.first_name + " " + self.last_name

