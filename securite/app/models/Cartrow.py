from django.db import models
from app.models import Cart, Product


class Cartrow(models.Model):
    """
    This class correspond to a row of a cart

    Attributes:
        cart_id (int): The ID of the cart
        product_id (Date): The last update date of the cart
        quantity (int): The quantity of product

    """
    cart = models.ForeignKey(Cart, on_delete=models.CASCADE)
    product = models.ForeignKey(Product, on_delete=models.CASCADE)
    quantity = models.IntegerField(default=1)
    objects = models.Manager()


    def __str__(self):
        return ""

