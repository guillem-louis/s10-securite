from django.db import models

from app.models import User


class Cart(models.Model):
    """
    This class is the base class for any user of the app. It should not be instanciated at any
    moment. Use CEO, AisleManager or StoreManager instead.

    Attributes:
        id (int): The ID of the cart that is incremented automatically by Django.
            It is the PK.
        user_id (int): The ID of the user
        last_update_date (Date): The last update date of the cart

    """
    id = models.AutoField(primary_key=True)
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    last_update_date = models.DateField
    objects = models.Manager()


    def __str__(self):
        return ""

