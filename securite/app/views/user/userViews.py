from django.shortcuts import render, get_object_or_404, redirect
from django.contrib.auth import authenticate, login as log_in, logout as log_out

from app.models import *
from app.services import *
import logging
logger = logging.getLogger("appLog")

def register(request):
    """

    :param request:
    :return:
    """
    return render(request, 'user/newUser.html')


def create(request):
    """

    :param request:
    :return:
    """
    try:
        email = request.POST['email']
        password = request.POST['password']
        first_name = request.POST['first-name']
        last_name = request.POST['last-name']
    except Exception as e:
        logger.error("Error creating user : invalid post param")
        return render(request, e, status=400)
    try:
        create_user_service(first_name=first_name, last_name=last_name, email=email, password=password)
    except Exception as e:
        logger.error("Error creating user")
        return render(request, 'user/newUser.html', status=400)
    context = {
        'message': "Account created"
    }
    return render(request, 'user/login.html', context=context)
