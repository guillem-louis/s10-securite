import requests
from app.models import *
from app.services import *
from cryptography.hazmat.backends import default_backend
from cryptography.hazmat.primitives import hashes
from cryptography.hazmat.primitives.asymmetric import padding
from cryptography.hazmat.primitives.serialization import load_pem_public_key
from django.shortcuts import render, redirect
from django.views.decorators.debug import sensitive_post_parameters
import logging
logger = logging.getLogger("appLog")

def payment(request):
    cart = Cart.objects.get(user_id=request.user.id)
    cartrows = Cartrow.objects.filter(cart_id=cart.id)
    total = 0
    for c in cartrows:
        c.total = c.product.unit_price * c.quantity
        total += c.total
    context = {
        "cartrows": cartrows,
        "total": total
    }
    return render(request, 'user/payment.html', context=context)


@sensitive_post_parameters('card_number')
def pay(request):
    cart = Cart.objects.get(user_id=request.user.id)
    cartrows = Cartrow.objects.filter(cart_id=cart.id)
    total = 0
    for c in cartrows:
        c.total = c.product.unit_price * c.quantity
        total += c.total
    context = {
        "error": "Erreur durant le paiement",
        "cartrows": cartrows,
        "total": total
    }
    try:
        card_number = request.POST['card_number']
    except Exception as e:
        logger.error("Payment failed : invalid post param")

        return render(request, "user/payment.html", status=400)

    response = requests.get("http://" + request.get_host() + "/payment/key/")
    if (response.status_code != 200):
        logger.error("Payment failed : can't get public key")
        return render(request, 'user/payment.html', context=context)

    else:
        pk = load_pem_public_key(response.json().encode(), backend=default_backend())
        encrypted = pk.encrypt(
            card_number.encode(),
            padding.OAEP(
                mgf=padding.MGF1(algorithm=hashes.SHA256()),
                algorithm=hashes.SHA256(),
                label=None
            )
        )
        with open('data', 'wb') as f:
            f.write(encrypted)
        files = {'file': open('data', 'rb')}
        response = requests.post("http://" + request.get_host() + "/payment/pay/", files=files)
        if response.status_code == 200:
            delete_user_cartrows(request.user.id)
            return  redirect('cart')
        else:
            logger.error("Payment failed ")

    return render(request, 'user/payment.html', context=context)
