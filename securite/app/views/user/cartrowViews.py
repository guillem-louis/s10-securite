from django.shortcuts import render

from app.models import *
from app.services import *
from app.decorators import auth_required
from app.services.CartService import add_cartrow_service
import logging
logger = logging.getLogger("appLog")

@auth_required
def create_cartrow(request):
    try:
        product_id = request.POST['product_id']
        retour = add_cartrow_service(id_user=request.user.id, id_product=product_id)
        logger.info("cartrow added")

        products = products = Product.objects.all()
        context = {
            'products': products,
            "message": "Product added to the cart",
        }
        return render(request, 'user/home.html', context=context, status=200)
    except Exception as e:
        logger.error("Error creating cartrow product")

        products = products = Product.objects.all()
        context = {
            'products': products,
            'message': 'We encountered a problem adding this product to your cart'
        }
        return render(request, 'user/home.html', context=context, status=404)
