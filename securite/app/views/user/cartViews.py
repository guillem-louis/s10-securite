from django.shortcuts import render, get_object_or_404, redirect
from django.contrib.auth import authenticate, login as log_in, logout as log_out

from app.models import *
from app.services import *
from app.decorators import auth_required
import logging
logger = logging.getLogger("appLog")

@auth_required
def cart(request):
    cart = Cart.objects.get(user_id=request.user.id)
    cartrows = Cartrow.objects.filter(cart_id=cart.id)

    context = {
        "cartrows": cartrows,
    }
    return render(request, 'user/cart.html', context=context)


@auth_required
def update_cartrow(request, cartrow_id):
    try:
        quantity = request.POST['quantity']
    except Exception as e:
        logger.error('Error update cartrow : fields missing')
        return render(request, "user/login.html", status=400)

    cartrow = Cartrow.objects.get(id=cartrow_id)
    if cartrow is None:
        return render(request, "user/login.html", status=400)
    cartrow.quantity = quantity
    cartrow.save()
    return redirect('cart')


@auth_required
def delete_cartrow(request, cartrow_id):
    cartrow = Cartrow.objects.get(id=cartrow_id)
    if cartrow is None:
        logger.error('Error deleting cartrow : inexisting cartrow')

        return render(request, "user/login.html", status=400)
    cartrow.delete()
    return redirect('cart')
