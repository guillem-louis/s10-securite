from django.shortcuts import render

from app.models import Product
from app.services.ProductService import search_product_service


def list_products(request):
    products = Product.objects.all()

    context = {
        'products': products,
    }

    return render(request, 'user/home.html', context=context)

def search_product(request):
    try:
        name = request.POST['name']
        price_min = request.POST['price-min']
        price_max = request.POST['price-max']
        category = request.POST['category']
    except Exception as e:
        return render(request, e, status=400)

    try:
        products = search_product_service(name, price_min, price_max, category)
        context = {
            "products": products
        }
        return render(request, 'user/home.html', context=context)
    except Exception as e:
        return render(request, e, status=400)

def adm_search_product(request):
    try:
        name = request.POST['name']
        price_min = request.POST['price-min']
        price_max = request.POST['price-max']
        category = request.POST['category']
    except Exception as e:
        return render(request, e, status=400)

    try:
        products = search_product_service(name, price_min, price_max, category)
        context = {
            "products": products
        }
        return render(request, 'admin/adminHome.html', context=context)
    except Exception as e:
        return render(request, e, status=400)