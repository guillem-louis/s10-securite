from django.shortcuts import render, get_object_or_404, redirect
from django.contrib.auth import authenticate, login as log_in, logout as log_out

from app.models import *
from app.decorators import *
import logging
logger = logging.getLogger("appLog")


def adminLogin(request):
    """

    :param request:
    :return:
    """
    return render(request, 'admin/adminLogin.html')


def adminLogout(request):
    """ Logout function

    This function renders login.html with a farewell message
    """
    if not request.user.is_authenticated:
        logger.error("User not authenticated, can't logout ")
        return render(request, 'admin/adminLogin.html', status=401)
    else:
        logger.info("User disconnected ")
        log_out(request)
    context = {
        'message': "Disconnected"
    }
    return render(request, 'admin/adminLogin.html', context=context)

def adminAuth(request):
    """ Admin authentication function

    It uses 'email' and 'password' as inputs names. If the user is correct,
    it renders 'home.html'. Otherwise it routes the user back to login with a
    'message' text in context.
    """
    try:
        email = request.POST['login']
        password = request.POST['password']
    except Exception as e:
        logger.error("Can't authenticate admin, invalid POST Fields ")
        return render(request, 'admin/adminLogin.html', status=400)

    user = authenticate(request, username=email, password=password)

    if user is not None and user.is_admin:
        log_in(request, user)
        products = Product.objects.all()

        context = {
            'products': products,
        }

        # display stores as home page
        return render(request, 'admin/adminHome.html', context=context )
    else:
        logger.error("Authentification failed : bad credentials")

        context = {
            'message': "Bad credentials"
        }
        return render(request, 'admin/adminLogin.html', context=context, status=401)

@admin_required
def adminHome(request):
    """ Home function
    This function renders the list of stores in DB in home.html. It calls get_stores_context().
    """
    products = Product.objects.all()
    context = {
        'products': products,
    }
    return render(request, 'admin/adminHome.html', context=context)
