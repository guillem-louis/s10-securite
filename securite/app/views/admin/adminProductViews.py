from django.shortcuts import render, get_object_or_404, redirect

from app.models import *
from app.services import *
from app.decorators import *
import logging
logger = logging.getLogger("appLog")

@admin_required
def create_product(request):
    try:
        name = request.POST['name']
        unit_price = request.POST['unit_price']
        category = request.POST['category']
    except:
        return render(request, 'admin/newProduct.html', status=400)
    try:
        create_product_service(name=name, unit_price=unit_price, category=category)
        logger.info("Product created")

    except Exception as e:
        logger.error("Error creating product")
        context = {
            'message': e
        }
        return render(request, 'admin/newProduct.html', context=context)
    products = Product.objects.all()
    context = {
        'products': products,
    }

    return render(request, 'admin/adminHome.html', context=context)


@admin_required
def product(request, product_id):
    try:
        product = Product.objects.get(id=product_id)
        context = {
            "product": product
        }
        return render(request, 'admin/product.html', context=context)
    except Exception as e:
        logger.error("Error can't get product")

        return render(request, 'admin/adminHome.html', status=400)


@admin_required
def update_product(request,product_id):
    try:
        name = request.POST['name']
        unit_price = request.POST['unit_price']
        category = request.POST['category']
        product = Product.objects.get(id=product_id)
    except:
        logger.error("Error updating product : invalid post fields")
        return render(request, 'admin/adminHome.html', status=400)
    try:
        update_product_service(product_id, name, category,unit_price)
    except Exception as e:
        logger.error("Error updating product")
        context = {
            'message': e,
            'product': product
        }
        return render(request, 'admin/product.html', context=context)
    products = Product.objects.all()
    context = {
        'products': products,
    }
    return render(request, 'admin/adminHome.html',context=context)


@admin_required
def new_product(request):
    return render(request, 'admin/newProduct.html')
