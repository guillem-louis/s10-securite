

from app.models import *
import logging
logger = logging.getLogger("appLog")

def add_cartrow_service(id_product, id_user):
    try:
        user = User.objects.get(id=id_user)
        cart = Cart.objects.get(user=user)
        product = Product.objects.get(id=id_product)
        try:
            cartrow = Cartrow.objects.get(cart=cart, product=product)
            cartrow.quantity = cartrow.quantity + 1

            cartrow.save()
        except Exception as e:

            logger.error("Can't get cartrow")

            cartrow = Cartrow.objects.create(cart=cart, product=product)
            cartrow.save()
        return cartrow.id
    except Exception as e:
        logger.error("Can't get cart with userId:" + str(id_user))
        raise Cartrow.DoesNotExist


def delete_user_cartrows(id_user):
    Cartrow.objects.filter(cart__user__id=id_user).delete()
