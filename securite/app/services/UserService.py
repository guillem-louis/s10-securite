from app.models import *
from django.views.decorators.debug import sensitive_variables
import logging
logger = logging.getLogger("appLog")

@sensitive_variables("first_name", "last_name", "email", "password")
def create_user_service(first_name, last_name, email, password):
    if first_name.isdigit() or not first_name or not last_name or not email or not password:
        logger.error("Error create user : fields are digits ")

        raise ValueError
    try:
        user = User.objects.create(first_name=first_name, last_name=last_name, email=email, password=password)
        user.save()
        cart = Cart.objects.create(user=user)
        cart.save()
        return user.id
    except:
        logger.error("Error create user ")
        return -1



