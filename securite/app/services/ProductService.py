import sys
from app.models import *
import logging
logger = logging.getLogger("appLog")

def create_product_service(name, category, unit_price):
    unit_price = unit_price.replace(',', '.')
    unit_price = float(unit_price)
    if not name.strip() or not category.strip() or not unit_price:
        logger.error("Error create product : empty fields")
        raise ValueError('empty fields')
    if  unit_price < 0:
        logger.error("Error create product : invalid price")
        raise ValueError('invalid unit price')
    try:
        product = Product.objects.create(name=name, category=category, unit_price=unit_price)
        product.save()
        return product.id
    except:
        logger.error("Error create product ")
        return -1

def update_product_service(id, name, category, unit_price):
    unit_price = unit_price.replace(',', '.')
    unit_price = float(unit_price)
    if not name.strip() or not category.strip() or not unit_price:
        logger.error("Error update product : empty fields")
        raise ValueError('empty fields')
    if  unit_price < 0:
        logger.error("Error create product: invalid price ")

        raise ValueError('invalid unit price')
    try:
        product = Product.objects.get(id=id)
        product.name = name
        product.unit_price = unit_price
        product.category = category
        product.save()
        return True
    except:
        logger.error("Error create product ")

        return False

def search_product_service(name, price_min, price_max, category):
    if price_min == '' and price_max == '' and name == '' and category == '':
        return Product.objects.all()
    elif price_min == '' and price_max == '':
        products = Product.objects.filter(name__contains=name, category__contains=category)
    else:
        if price_min =='' :
            price_min = 0
        if price_max == '':
            price_max = sys.float_info.max
        products = Product.objects.filter(name__contains=name, category__contains=category, unit_price__gte=float(price_min), unit_price__lte=float(price_max))

    return products
